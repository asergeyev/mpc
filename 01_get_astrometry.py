import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
from rich import print

from astroquery.mpc import MPC
from astropy.coordinates import SkyCoord
from astropy import units as u

import ssptools

def get_miriade(observatory, observatories, n=10) :
    for i, file in enumerate(observatories):
        # print(file)
        df_obs = pd.read_parquet(file,
            columns=['epoch', 'RA_x', 'DEC_x', 'name', 'number']
                    )
        if i == 0:
            df_obs_all = df_obs
        else:
            df_obs_all = pd.concat([df_obs_all, df_obs])

    # Selecting only objects with more than 10 observations    
    df_val = df_obs_all.value_counts('number')
    df_val = df_val[df_val >= n]

    # Getting the data from Miriade for each asteroid
    for ast_number in df_val.index[:]:
        file_path = os.path.join(f'./data/astrometry/{observatory}/', f'{ast_number:06d}.csv')
        if os.path.exists(file_path):
            print(f'[red]{file_path} exists.[/red]')
            continue

        cond = df_obs_all['number'] == ast_number
        df_mpc = df_obs_all[cond].reset_index(drop=True)
        epoch_list = df_mpc['epoch'].to_list()
        print(f'Getting {len(df_mpc)} the {ast_number} data for {observatory} ...')
        try:
            # print(observatory, ast_number)
            df_miriade = ssptools.ephemcc(ident=ast_number, ep=epoch_list,
                                          observer=observatory)
            coords = SkyCoord(ra=df_miriade['RA'].values, 
                            dec=df_miriade['DEC'].values, 
                    unit=(u.hourangle, u.deg))
            df_miriade['RA_z'] = coords.ra.deg
            df_miriade['DEC_z'] = coords.dec.deg
            df_miriade = df_miriade[['RA_z', 'DEC_z', 'VMag']]
        except Exception as e:
            print(f'[blue]Error getting Miriade data for {ast_number}.[/blue]')
            print(e)
            continue
        if type(df_miriade) == bool:
            print(f'[blue]No Miriade data for {ast_number}.[/blue]')
            continue
        print(f'Done. {df_miriade.shape[0]} observations.')
        df_ast = pd.merge(df_mpc, df_miriade, left_index=True, right_index=True)
        df_ast['RA_x'] = df_ast['RA_x'].astype(float).round(6)
        df_ast['DEC_x'] = df_ast['DEC_x'].astype(float).round(6)
        df_ast['RA_z'] = df_ast['RA_z'].astype(float).round(6)
        df_ast['DEC_z'] = df_ast['DEC_z'].astype(float).round(6)
        df_ast['VMag'] = df_ast['VMag'].astype(float).round(3)

        df_ast.to_csv(file_path, index=False)
        # print(f'Saved {file_path}.')
       

if __name__ == '__main__':
    df_observatory_band = pd.read_csv('./data/df_observatory_band.csv', index_col=[0, 1])
    obs_tab = MPC.get_observatory_codes()
    dict_obs = {}
    for key, val in obs_tab['Code', 'Name']:
        dict_obs[key] = val

    # Gathering all observations
    path = os.path.join(f'./data/observatories/', 'observations_init')
    scan = os.scandir(path)
    file_list = [f.path for f in scan if f.is_file() and f.name.endswith('.parquet')]
    file_list.sort()

    idx = 1
    for observatory in df_observatory_band.index.levels[0].values[idx:idx+10]:
        # Selecting only the same observatory data
        if not os.path.exists(f'./data/astrometry/{observatory}/'):
            os.makedirs(f'./data/astrometry/{observatory}/')

        observatories = [x for x in file_list if f'_{observatory}_' in x]
        get_miriade(observatory, observatories, n=20)

