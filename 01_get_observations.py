# %%
import pandas as pd
import time
from astroquery.mpc import MPC
from astroquery.imcce import Miriade
import numpy as np
import matplotlib.pyplot as plt
from astropy.time import Time
from astropy.coordinates import SkyCoord
import requests
from astropy.io.votable import parse
import ssptools
import os
from scipy.optimize import curve_fit
from sbpy.photometry import HG1G2, HG
from astropy.stats import sigma_clip
from rich import print
from astropy import units as u
# todo:
# compare mode with ska predictions
# compare mode with inner (observatory) band to V band transformation
# %%
def getting_observatory_codes():
    obs_tab = MPC.get_observatory_codes()
    dict_obs = {}
    for key, val in obs_tab['Code', 'Name']:
        # print(key, val)
        dict_obs[key] = val
    return dict_obs

def group_data(df, is_save=False):
    df_gr = df.groupby(['band', 'observatory']).size().reset_index(name='count')
    cond = df_gr['count'] > 0
    df_gr = df_gr[cond].sort_values(by='count', ascending=False)
    df_gr['observatory_name'] = df_gr['observatory'].map(dict_obs)
    if is_save:
        df_gr.to_csv('./data/mpc_filters_iau_group.csv', index=False)

    return df_gr

def processing_observations(df, min_observations=40, isshow=False):
    # list_of_bad_observatories = ['C57', '084', 'Y00']
    
    # set band to 0 for unknown band
    cond = df['band'].isnull()
    df.loc[cond, 'band'] = '0'
    
    # remove TESS band and observation without magnitudes
    cond = ~df['mag'].isnull() # & \
    # ~df['observatory'].isin(list_of_bad_observatories) 

    # gathering observations with more than min_observations
    df_obs = df[cond].value_counts(['band', 'observatory'])
    df_obs = df_obs[df_obs > min_observations]
    df = df[cond].reset_index(drop=True)

    if isshow:
        for band, observatory in df_obs.index:
            print(band, observatory)
            cond = (df['band'] == band) & (df['observatory'] == observatory)
            df[cond]['mag'].hist(alpha=0.5, bins=20, 
                label=f'{band} {observatory}', range=(15, 22))
        plt.legend()
    plt.show()

    return df_obs, df

def plot_phase(df, i, l=1):
    for band, observatory in df_obs.index[i:i+l]:
        print(band, observatory)
        cond = (df['band'] == band) & (df['observatory'] == observatory)
        plt.scatter(df.loc[cond, 'Phase'], 
                    df.loc[cond, 'mag_corr'],
                    marker='.',
                    c=df.loc[cond, 'epoch'],
                    cmap='jet',
                    label=f'{band} {observatory}'
                    )
    # plt.legend()
    plt.title(f'{idx}: {band}, {dict_obs[observatory]}')
    plt.gca().invert_yaxis()
    plt.grid()
    plt.show()

def plot_phase2(df, i, l=1):
    for band, observatory in df_obs.index[i:i+l]:
        print(band, observatory, df_obs.loc[band, observatory], dict_obs[observatory])
        cond = (df['band'] == band) & (df['observatory'] == observatory)
        plt.scatter(df.loc[cond, 'Phase'], 
                    df.loc[cond, 'mag_corr'],
                    marker='.',
                    color='k',
                    # c=df.loc[cond, 'epoch'],
                    # cmap='jet',
                    # label=f'{band} {observatory}'
                    )
        
        plt.scatter(df.loc[cond, 'Phase'], 
                    df.loc[cond, 'mag_corrV'] + 1.0,
                    marker='.',
                    # color='r',
                    # c=df.loc[cond, 'epoch'],
                    # cmap='jet',
                    label=f'{band}: {observatory}'
                    )
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    # plt.legend()
    plt.title(f'{idx}')
    plt.gca().invert_yaxis()
    plt.grid()
    plt.show()


def get_mode(df, isshow=False):
    # i, l = 0, 100
    # df_mode = pd.DataFrame()
    for band, observatory in df_obs.index:
        cond = (df['band'] == band) & (df['observatory'] == observatory)
        hist, bin_edges = np.histogram(df.loc[cond, 'mag'] - df.loc[cond, 'VMag'], 
                                    bins=40, density=True, range=(-1, 1)
                                    )
        w = bin_edges[1] - bin_edges[0]
        mode = bin_edges[np.argmax(hist)]+w/2

        if isshow:
            plt.bar(bin_edges[:-1]+w/2, hist, width=w, alpha=0.5,
                    label=f'{band} {observatory}')
            plt.axvline(mode, color='k', linestyle='--')
            print(f'{band} {observatory} {dict_obs[observatory]} {len(df[cond])}: mode= {mode:.2f}')

        df.loc[cond, 'mag_corrV'] = df.loc[cond, 'mag'] - 5 * \
            np.log10(df.loc[cond, 'Dhelio'] * df.loc[cond, 'Dobs']) - mode

    if isshow:
        # plt.legend()
        plt.title(f'{idx}')
        plt.legend()
        plt.grid()
        plt.tight_layout()
        plt.show()

    return df

# Define the H, G phase function
def hg_phase_function(alpha, H, G):
    # Phase functions Phi1 and Phi2
    # W = np.exp(-3.33 * np.tan(alpha/2)**0.63)
    # R = np.exp(-1.87 * np.tan(alpha/2)**1.22)
    # return H - 2.5 * np.log10((1 - G) * W + G * R)

    # Standard G part
    func1 = (1 - G) * HG._hgphi(alpha, 1) + G * HG._hgphi(alpha, 2)
    func1 = -2.5 * np.log10(func1)

    return H + func1

# Define the H, G1, G2 phase function
def hg1g2_phase_function(alpha, H, G1, G2):
    # tan_half_alpha = np.tan(alpha / 2)
    # phi1 = np.exp(-3.332 * tan_half_alpha ** 0.631)
    # phi2 = np.exp(-1.862 * tan_half_alpha ** 1.218)
    # phi3 = np.exp(-1.218 * tan_half_alpha ** 2.591)
    # return H - 2.5 * np.log10(G1 * phi1 + G2 * phi2 + (1.0 - G1 - G2) * phi3)

    func1 = G1 * HG1G2._phi1(alpha) + G2 * HG1G2._phi2(alpha) +\
        (1.0 - G1 - G2) * HG1G2._phi3(alpha)
    func1 = -2.5 * np.log10(func1)
    return H + func1

def compute_phase_parameters(df_ast, df_obs, isshow=False):
    phase_angles = []
    magnitudes = []
    for (band, obs) in df_obs.index[:]:
        cond = ((df_ast['band'] == band) & (df_ast['observatory'] == obs)
                & df_ast['mag_corrV'].notnull())

        phase_angles.extend(df_ast.loc[cond, 'Phase'].to_list())
        magnitudes.extend(df_ast.loc[cond, 'mag_corrV'].to_list())

    # Convert phase angles to radians
    phase_angles_rad = np.radians(phase_angles)
    magnitudes = np.array(magnitudes)
            
    # Initial guess for H and G
    initial_guess = [np.median(magnitudes), 0.2] # Initial guess
    # Fit the H, G phase function to the observed data
    best_fit, covariance = curve_fit(hg_phase_function, phase_angles_rad, 
                                    magnitudes, p0=initial_guess)
    H_fit, G_fit = best_fit
    # Calculate standard deviations (sqrt of the diagonal of the covariance matrix)
    H_std, G_std = np.sqrt(np.diag(covariance))
    print(f"Best fit H: {H_fit:.4f} +/- {H_std:.4f}")
    print(f"Best fit G: {G_fit:.4f} +/- {G_std:.4f}")
    print()

    residual_data = magnitudes - hg_phase_function(phase_angles_rad, H_fit, G_fit)
    residual_data = magnitudes - hg_phase_function(phase_angles_rad, H_fit, G_fit)
    filtered_data, min_bound, max_bound = \
        sigma_clip(residual_data, sigma=3, return_bounds=True,
        maxiters=10, masked=True, cenfunc='median')

    # Initial guess for H, G1, and G2
    initial_guess = [H_fit, 0.1, 0.1]
    # Fit the H, G1, G2 phase function to the observed data

    masked_phase_angles_rad = phase_angles_rad[~filtered_data.mask]
    masked_magnitudes = magnitudes[~filtered_data.mask]

    # Fit the H, G1, G2 phase function to the observed data
    best_fit1, covariance1 = curve_fit(hg1g2_phase_function, 
            masked_phase_angles_rad, masked_magnitudes, 
            p0=initial_guess, maxfev=5000)
    H1_fit, G1_fit, G2_fit = best_fit1

    # Calculate standard deviations (sqrt of the diagonal of the covariance matrix)
    H1_std, G1_std, G2_std = np.sqrt(np.diag(covariance1))
    print(f"Best fit H: {H1_fit:.4f} +/- {H1_std:.4f}")
    print(f"Best fit G1: {G1_fit:.4f} +/- {G1_std:.4f}")
    print(f"Best fit G2: {G2_fit:.4f} +/- {G2_std:.4f}")

    # Generate a range of phase angles for the model curve
    model_phase_angles = np.linspace(0, np.max(phase_angles_rad), 100)

    # Calculate model magnitudes using the fitted H and G values
    model_magnitudes = hg_phase_function(model_phase_angles, H_fit, G_fit)
    # Calculate uncertainties for the fitted model
    model_magnitudes_upper = hg_phase_function(model_phase_angles, H_fit + H_std, G_fit + G_std)
    model_magnitudes_lower = hg_phase_function(model_phase_angles, H_fit - H_std, G_fit - G_std)

    # Calculate model magnitudes using the fitted H, G1, and G2 values
    model_magnitudes = hg1g2_phase_function(model_phase_angles, H1_fit, G1_fit, G2_fit)
    # Calculate uncertainties for the fitted model
    model_magnitudes_upper1 = hg1g2_phase_function(model_phase_angles, H1_fit + H1_std, G1_fit + G1_std, G2_fit + G2_std)
    model_magnitudes_lower1 = hg1g2_phase_function(model_phase_angles, H1_fit - H1_std, G1_fit - G1_std, G2_fit - G2_std)

    # Actual observations
    plt.scatter(phase_angles, magnitudes, 
                marker='.', edgecolors='none', facecolors='k',
                alpha=1.0,
                label='Observed Data')
    plt.scatter(masked_phase_angles_rad * 180 /np.pi, masked_magnitudes, 
                marker='.', edgecolors='none', facecolors='r',
                alpha=1.0,
                label='Masked Data')

    # Fitted curve
    plt.plot(np.degrees(model_phase_angles), model_magnitudes,
            label=f'Fitted curve, H:{H_fit:.2f}$\pm${H_std:.2f}, \n G:{G_fit:.2f}$\pm${G_std:.2f}',
            color='r', lw=1
            )
    # Fitted curve
    plt.plot(np.degrees(model_phase_angles), model_magnitudes,
            label=f'Fitted curve, H:{H1_fit:.2f}$\pm${H1_std:.2f}, \n G1:{G1_fit:.2f}$\pm${G1_std:.2f}, G2:{G2_fit:.2f}$\pm${G2_std:.2f}',
            color='b', lw=1
            )

    # Uncertainty region
    plt.fill_between(np.degrees(model_phase_angles), model_magnitudes_lower, 
                    model_magnitudes_upper, color='red', alpha=0.2, 
                    # label='Uncertainty'
                    )

    plt.fill_between(np.degrees(model_phase_angles), model_magnitudes_lower1, 
                    model_magnitudes_upper1, color='blue', alpha=0.2, 
                    label='HG1G2 Uncertainty')

    # Labels and title
    plt.xlabel('Phase Angle (degrees)')
    plt.ylabel('Magnitude')
    plt.title(f'H, G Phase Function Fitting of Asteroid {idx}')
    # plt.legend()
    plt.legend(loc='center left', bbox_to_anchor=(0.5, 0.80))
    plt.ylim(np.ma.min(np.ma.array(magnitudes, mask=filtered_data.mask))-0.2, 
            np.ma.max(np.ma.array(magnitudes, mask=filtered_data.mask))+0.3)
    plt.gca().invert_yaxis()
    plt.show()

    # plot residuals
    x = phase_angles
    y = magnitudes - hg1g2_phase_function(phase_angles_rad, H1_fit, G1_fit, G2_fit)
    mean, std = np.mean(y), np.std(y)

    print(f'mean: {mean:.3f}, std: {std:.3f}')
    plt.scatter(x, y,
                marker='.', edgecolors='none', facecolors='k',
                alpha=0.5,
                label='Residuals std: {:.3f} mag'.format(std),
                )
    plt.axhline(mean, color='r', linestyle='-')
    plt.axhline(min_bound, color='r', linestyle='--')
    plt.axhline(max_bound, color='r', linestyle='--')
    plt.annotate(f'sigma clip bound: {max_bound:.3f}', 
                 xy=(0, min_bound-0.02), color='r')

    plt.axhline(mean + std, color='b', linestyle='--')
    plt.axhline(mean - std, color='b', linestyle='--')
    plt.annotate(f'std: {std:.3f}', xy=(0, -std -0.02), color='b')

    plt.xlabel('Phase Angle (degrees)')
    plt.ylabel('Magnitude')
    plt.ylim(2 * min_bound, 2 * max_bound)
    plt.title(f'H, G1, G2 Phase Function Residuals of Asteroid {idx}')
    plt.legend()
    plt.gca().invert_yaxis()
    plt.show()


def moving_files():
    init_folder = './data/phase/'
    scan = os.scandir(init_folder)
    file_list = [x.name for x in scan if x.name.endswith('.csv')]
    file_list.sort()
    for file in file_list:
        file_number = int(file.split('.')[0])
        out_folder = os.path.join(init_folder, f'{file_number//10_000*10_000:06d}')
        out_file = os.path.join(out_folder, file)
        if not os.path.exists(out_folder):
            os.mkdir(out_folder)
        init_file = os.path.join(init_folder, file)
        print(f'{init_file} -> {out_file}')
        os.rename(init_file, out_file)

# %%
if __name__ == '__main__':
    # getting asteroid observation from mpc
    dict_obs = getting_observatory_codes()
    idx = 33931 
    # idx = 8467
    # idx = 8786 # 
    # idx = 17036 
    # idx = 30435
    # idx = 34583
    idxs = np.random.choice(600_000, 10_000)

    # for idx in range(120_000, 120_200):
    for idx in idxs:
        out_folder = f'./data/phase/{idx//10_000*10_000:06d}/'
        
        if not os.path.exists(out_folder):
            os.makedirs(out_folder)

        file_path = os.path.join(out_folder, f'{idx:06d}.csv')
        if os.path.exists(file_path):
            print(f'[red]{file_path} exists.[/red]')
            # df_ast = pd.read_csv(f'./data/phase/{idx:06d}.csv')
            # df_obs, df_mpc = processing_observations(df_ast)
        else:
            print(f'Getting Asteroid {idx} the data from MPC ...')
            try:
                df_mpc = MPC.get_observations(targetid=idx).to_pandas()
            except:
                print(f'[red]Error getting MPC data for {idx}.[/red]')
                continue
            print(f'Done. {df_mpc.shape[0]} observations.')
            
            df_obs, df_mpc = processing_observations(df_mpc)
            epoch_list = df_mpc['epoch'].to_list()
            print(f'Getting {len(df_mpc)} the data from Miriade ...')
            try:
                df_miriade = ssptools.ephemcc(idx, epoch_list)
                coords = SkyCoord(ra=df_miriade['RA'].values, 
                                  dec=df_miriade['DEC'].values, 
                      unit=(u.hourangle, u.deg))
                df_miriade['RA_z'] = coords.ra.deg
                df_miriade['DEC_z'] = coords.dec.deg
            except Exception as e:
                print(f'[blue]Error getting Miriade data for {idx}.[/blue]')
                print(e)
                continue
            if type(df_miriade) == bool:
                print(f'[blue]No Miriade data for {idx}.[/blue]')
                continue
            print(f'Done. {df_miriade.shape[0]} observations.')
            df_ast = pd.merge(df_mpc, df_miriade, left_index=True, right_index=True)
            df_ast['mag_corr'] = df_ast['mag'] - 5 * \
                np.log10(df_ast['Dhelio'] * df_ast['Dobs'])
            # Computing mode offset of V band for each band and observatory
            df_ast = get_mode(df_ast, isshow=False)
            df_ast.to_csv(file_path, index=False)

        # compute_phase_parameters(df_ast, df_obs)
# %%
# df_gr = group_data(df_ast)
# df_ast = pd.read_csv(f'./data/phase/{idx:06d}.csv')
# df_obs = get_observations_to_process(df_ast, isshow=True)
# plot_phase(df_ast, 0, l=100)
# plot_phase2(df_ast, 0, l=100)
# compute_phase_parameters(df_ast, df_obs)


# # %%
# # H mag to asteroid diameter
# H = 13.4605
# D = 1329 / np.sqrt(0.1) * 10**(-0.2 * H)
# print(D)
# # %%
# phase_angles = []
# magnitudes = []
# for (band, obs) in df_obs.index[:]:
#     cond = (df_ast['band'] == band) & (df_ast['observatory'] == obs)
#     phase_angles.extend(df_ast.loc[cond, 'Phase'].to_list())
#     magnitudes.extend(df_ast.loc[cond, 'mag_corrV'].to_list())

# # Convert phase angles to radians
# phase_angles_rad = np.radians(phase_angles)
# magnitudes = np.array(magnitudes)
        
# # Initial guess for H and G
# initial_guess = [np.median(magnitudes), 0.2] # Initial guess
# # Fit the H, G phase function to the observed data
# best_fit, covariance = curve_fit(hg_phase_function, phase_angles_rad, 
#                                 magnitudes, p0=initial_guess)
# H_fit, G_fit = best_fit
# # Calculate standard deviations (sqrt of the diagonal of the covariance matrix)
# H_std, G_std = np.sqrt(np.diag(covariance))
# print(f"Best fit H: {H_fit:.4f} +/- {H_std:.4f}")
# print(f"Best fit G: {G_fit:.4f} +/- {G_std:.4f}")
# print()

# residual_data = magnitudes - hg_phase_function(phase_angles_rad, H_fit, G_fit)
# filtered_data, min_bound, max_bound = \
#     sigma_clip(residual_data, sigma=3, return_bounds=True,
#     maxiters=10, masked=True, cenfunc=np.median)
# filtered_data
# # %%
# plt.scatter(phase_angles, filtered_data.data)
# plt.scatter(phase_angles, filtered_data)
# plt.axhline(min_bound, color='r', linestyle='--')
# plt.axhline(max_bound, color='r', linestyle='--')

# # %%

# %%
