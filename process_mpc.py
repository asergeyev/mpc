# %%
import pandas as pd
import time
from astroquery.mpc import MPC

def fixing_mpcatobs():
    f_in = open('./data/mpcatobs.csv', 'rt')
    f_out = open('./data/mpcatobs_clean.csv', 'wt')
    t0 = time.time()
    for i, line in enumerate(f_in):
        ls = line.strip().split(',')
        l = len(ls)
        if l == 26:
            f_out.write(','.join(ls) + '\n')
        else:
            f_out.write(','.join(ls[:17]) + ',' + ','.join(ls[-9:]) + '\n')
        if i % 1_000_000 == 0:
            print(i, f"{time.time()-t0:.2f}")
            t0 = time.time()
            print(i)

    f_in.close()
    f_out.close()

def load_data_chank(usecols, chunksize=1_000_000):
    def process_chunk(chunk):
        return chunk['filter'].value_counts().fillna(0)
    
    def group_chunk(chunk):
        df = chunk.groupby(['filter', 'iau_code']).size().reset_index(name='count')
        return df

    with pd.read_csv('./data/mpcatobs_clean.csv', 
            chunksize=chunksize, usecols=usecols) as reader:
        t0 = time.time()
        for chunk in reader:
            print(f'Read completed: {time.time()-t0:.2f}, processing chunk {i}')
            if i == 0:
                df_filters = process_chunk(chunk)
            else:
                df_filters = df_filters.add(process_chunk(chunk), fill_value=0)
            i += 1
            print(i, f"{time.time()-t0:.2f}")
            t0 = time.time()

    df_filters.to_csv('./data/mpc_filters_number.csv', index=False)

def load_data(usecols):
    df = pd.read_csv('./data/mpcatobs_clean.csv', 
                      usecols=usecols,
                      )
    return df

def save_data(df, fname):
    print(f'Saving the data to {fname} ...')
    df['filter'] = df['filter'].astype('category')
    df['iau_code'] = df['iau_code'].astype('category')
    df.to_parquet(fname, index=False)
    print('Done.')

def getting_observatory_codes():
    obs_tab = MPC.get_observatory_codes()
    dict_obs = {}
    for key, val in obs_tab['Code', 'Name']:
        print(key, val)
        dict_obs[key] = val
    return dict_obs

def group_data(df, is_save=True):
    dict_obs = getting_observatory_codes()

    df_gr = df.groupby(['filter', 'iau_code']).size().reset_index(name='count')
    cond = df_gr['count'] > 0
    df_gr = df_gr[cond].sort_values(by='count', ascending=False)
    df_gr['observatory'] = df_gr['iau_code'].map(dict_obs)
    if is_save:
        df_gr.to_csv('./data/mpc_filters_iau_group.csv', index=False)

    return df_gr

if __name__ == '__main__':
    # fixing the csv file
    fixing_mpcatobs() 

    # loading the data
    usecols = ['filter', 'iau_code']
    df = load_data(usecols)
    print(df.info())

    # saving the data
    save_data(df, "./data/mpc_filters_iau_cat.parquet")

    # group by filter and iau_code
    df_gr = group_data(df)


# %%


