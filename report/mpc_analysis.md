# Analysis of Asteroid Observation Data from MPC

In this report, we will analyze the observation data obtained from the Minor Planet Center (MPC) for asteroids. The MPC is responsible for collecting and archiving observations of asteroids and other minor planets. By studying this data, we can gain insights into the characteristics and behavior of asteroids, which can be valuable for various scientific and practical purposes.

In this analysis, we will explore the available data, perform statistical calculations, visualize the findings, and draw conclusions based on the results.

# The analisys of sample that were downloaded from MPC




