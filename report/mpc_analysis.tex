\documentclass[a4paper]{article}

% Packages
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{longtable} % In case the descriptions are long and might span multiple pages
\usepackage{booktabs} % For professional quality tables

% Document information
\title{Analysis of Asteroid Photometry Data from Minor Planet Center (MPC)}
\author{Alexey Sergeyev}
\date{\today}

% Document content
\begin{document}

\maketitle

\section{Introduction}
% Your content here
In this study, we comprehensively analyze observational data about asteroids, as collated and maintained by the Minor Planet Center (MPC). 
The MPC serves as a crucial repository for accumulating and archival observational records of asteroids and other minor planetary bodies. 
By meticulously examining this dataset, we aim to elucidate the intrinsic properties and uncertainties of the photometry data obtained from various sources. 
This endeavor holds significant implications for a wide array of scientific inquiries and pragmatic applications.
The scope of our analysis encompasses the systematic exploration of the dataset, execution of statistical analyses, graphical representation of our findings, and the formulation of conclusions grounded in the empirical evidence obtained.

\section{Methodology}
% Your content here
In this study, the data sample analyzed was derived from observations of approximately 10 percent of asteroids randomly selected from those assigned number codes within the range of 1 to 600,000. This selection process was designed to ensure a representative and diverse sample of the asteroid population under investigation. The methodology for data acquisition involved the extraction of observational records specifically for these randomly chosen asteroids, facilitating a comprehensive analysis. The chosen sample span, encompassing a broad spectrum of number codes, aimed to reflect the varied characteristics and behaviors of the asteroid population within the specified range. This approach allowed for a more nuanced understanding of the data, laying the groundwork for subsequent analysis and interpretation.

The data was stored in a series of CSV files, each containing observational records for individual asteroids. These files were organized into multiple rows, with each row corresponding to a specific observation. The columns of the CSV files encapsulated a variety of observational metrics and identifiers, including the asteroid number, provisional designation, observational epoch and various other relevant parameters (see \ref{tab:asteroid_columns}). The dataset was meticulously curated and compiled to ensure the inclusion of comprehensive and accurate observational data, thereby facilitating a robust and reliable analysis.

\begin{longtable}{lp{10cm}}
    \toprule
    \textbf{Column} & \textbf{Description} \\
    \midrule
    \endhead
    Number & The unique identifier assigned to the asteroid by the MPC. \\
    desig & The provisional designation given to the asteroid at the time of its discovery or observation. \\
    discovery & A flag indicating whether the observation contributed to the asteroid's discovery (specific coding may vary). \\
    note1 & Miscellaneous notes or flags from the MPC regarding the observation. \\
    note2 & Additional miscellaneous notes or flags from the MPC. \\
    epoch & The Julian Date of the observation.\\
    RA\_x & Right Ascension of the asteroid at the middle of the observation, in degrees, from the MPC data. \\
    DEC\_x & Declination of the asteroid at the middle of the observation, in degrees, from the MPC data. \\
    mag & The observed magnitude of the asteroid. \\
    band & The photometric band in which the observation was made. \\
    observatory & The code of the observatory where the observation was made. \\
    Date & The date of the observation, typically in YYYY-MM-DD format. \\
    RA\_y & Predicted Right Ascension data from the IMCC Miriad database. \\
    DEC\_y & Predicted Declination data from the IMCC Miriad database. \\
    Dobs & The observational distance from Earth to the asteroid at the time of observation, in astronomical units (AU). \\
    Dhelio & The heliocentric distance of the asteroid, or its distance from the Sun, in AU. \\
    VMag & The apparent magnitude of the asteroid in the V-band, from Miriad data. \\
    Phase & The phase angle of the asteroid at the time of observation, in degrees. \\
    Elong. & The elongation of the asteroid, or its angular distance from the Sun as seen from Earth, in degrees. \\
    dRAcosDEC & The apparent motion in Right Ascension, corrected for declination. \\
    dDEC & The apparent motion in Declination. \\
    RV & The radial velocity of the asteroid, in km/s, indicating its speed towards or away from the observer, from Miriad data. \\
    mag\_corr & The corrected for the Sun and Earth distances observed magnitude of the asteroid. \\
    mag\_corrV & The corrected for the Sun and Earth distances the V-band magnitude of the asteroid. \\
    \bottomrule
    \caption{Description of the columns in the dataset}
    \label{tab:asteroid_columns}
    \end{longtable}

\begin{figure}[h]
\centering
\includegraphics[width=1.0\textwidth]{../figs/numbers_distribution.png}
\caption{Distribution of sampled asteroids by number code}
\label{fig:asteroid_distribution}
\end{figure}

The figure \ref{fig:asteroid_distribution} illustrates the distribution of sampled asteroids by number code.
Vertical lines, varying in opacity based on relative frequency within each bin, represent the density of presented asteroids in the sample.
Darker lines signify a higher asteroid density, with zero opacity representing 10 percent of the total asteroid count.
The horizontal axis represents the number code of the asteroids, while the vertical axis represents the number of asteroids within each bin.


\section{Data Exploration}

Our analysis commences with a thorough exploration of the dataset, focusing on the distribution of data points and the interactions between variables. We examined the relationship between asteroid number codes and the average number of observations per asteroid. The outcomes of this investigation are illustrated in the figure that follows.

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{../figs/obs_mean_number.png}
\caption{Distribution of observations per asteroid}
\label{fig:observations_distribution}
\end{figure}


The figure \ref{fig:observations_distribution} displays a bar chart that shows the percentage of asteroid observations contributed by various observatories to the dataset. The x-axis lists the observatory codes, and the y-axis indicates the percentage of total observations each observatory has made.

Observatories are identified by their unique codes, such as "G96" for Mt. Lemmon Survey, "F51" for Pan-STARRS 1, Haleakala, and "703" for Catalina Sky Survey, among others. The bars' height represents the proportion of observations attributed to each observatory, with the highest bars corresponding to observatories that contributed the most observations.

\begin{figure}[h]
\centering
\includegraphics[width=1.0\textwidth]{../figs/observatories_number.png}
\caption{Distribution of observations per observatory}
\label{fig:observatories_distribution}
\end{figure}

The figure \ref{fig:observatories_filters_distribution} provides the proportional contributions of different observatories to the overall collection of MPC asteroid observations along with the filters they utilize. Each bar corresponds to a specific observatory, identified by a unique code and the observation filter, and illustrates the percentage of total observations that the telescope contributes to the overall dataset. The presence of a '0' symbol denotes a lack of data regarding the observation's wavelength range.

\begin{figure}[h]
\centering
\includegraphics[width=1.0\textwidth]{../figs/observatories_filters_number.png}
\caption{Distribution of observations per observatory and filter}
\label{fig:observatories_filters_distribution}
\end{figure}

The figure \ref{fig:observations_year_distribution} is a bar chart that presents the number of asteroid observations over a while, from around 1980 to the  current year. The y-axis is on a logarithmic scale, indicating an exponential increase in the number of observations. 

From the chart, we can observe a significant increase in the number of asteroid observations as time progresses. The growth becomes notably steep from the early 2000s onward, suggesting a surge in asteroid observation activities during this period, possibly due to advancements in observational technology, increased interest in asteroid tracking, or more observatories participating in such surveys.

The period of growth seems to plateau slightly after 2010, indicating a stabilization in the rate of observations or perhaps the reaching of a maximum capacity of current observational technologies and programs. This leveling off could also reflect a saturation point in data collection or a shift in focus to more targeted observation strategies.

\begin{figure}[h]
\centering
\includegraphics[width=1.0\textwidth]{../figs/obs_year_number.png}
\caption{Distribution of observations per year}
\label{fig:observations_year_distribution}
\end{figure}

\section{Results}

\subsection{Photometry analysis}

To analyze the photometric data, we compared the observed magnitudes of asteroids with the predicted magnitudes. The predicted magnitudes were calculated using the IMCC Miriad ephemeris service, which provides ephemeris data for asteroids and other celestial bodies. The ephemeris data includes the predicted magnitudes of asteroids at the time of observation, as well as the distances to the Sun and Earth, the phase of the asteroid, and other relevant information.

   
\begin{figure}[h]
\centering
\includegraphics[width=1.0\textwidth]{../figs/bands_correction.png}
\caption{Distribution of magnitude differences for different observatories and filters}
\label{fig:observatories_filters_magnitude_difference_distribution}
\end{figure}

We computed the distribution of the difference between observed and predicted magnitudes ($\Delta mags$) for each observatories and filters. 
The provided figure \ref{fig:observatories_filters_magnitude_difference_distribution} appears to be a series of overlapping histograms or density plots, each representing the distribution of corrected magnitude differences ($\Delta mags$) for asteroids as observed by different observatories and in various photometric bands.
Each subplot corresponds to a different observatory, identified by a code (e.g., "704," "F51," "G96"), and shows distributions for different filters, which are indicated by photometric band designations (e.g., "V" for visual, "G" for green, "R" for red, "w" for wide-band). The dashed lines represent the fitting of a normal distribution to the data, with the mean and standard deviation indicated in the legend. These values suggest the bias or systematic shift in the magnitude measurements with the given filters.

From a brief analysis, the following observations can be made:
\begin{itemize}
    \item 
    The distributions of the magnitude differences ($\Delta mags$) for different observatories and filters follow the normal distribution, and therefore cold be corrected by subtracting the mean value.

    \item
    The spread (width) of each distribution varies, with some being quite narrow, indicating precise measurements, and others being wider, suggesting greater variability in the data.

    \item
    The numerical values indicate that some observatories and filters have greater uncertainties than others, which could be due to various factors including the sensitivity of the equipment, the data processing techniques, or the observational conditions.
\end{itemize}

\begin{longtable}{lp{10cm}}
    \toprule
    \textbf{Column} & \textbf{Description} \\
    \midrule
    \endhead
    observatory & The code identifying the observatory where observations were made. \\
    band & The photometric band of the observation, 0 - indicates no information of the used band \\
    mean & The mean value of the difference between observed and predicted V-band magnitudes.
    \\
    std & The standard deviation of magnitude difference between observed and predicted values. \\
    n\_obs & Number of observations contributing to the mean and standard deviation. \\
    % \bottomrule
    % \caption{Description of the band_correction CSV file.}
    % \label{tab:observatory_band_magnitude}
    \bottomrule
    \caption{Description of the band correction CSV file.}
    \label{tab:observatory_band_magnitude}
\end{longtable}

The data is stored in the band\_correction.csv file, which contains 79 rows. Each row of this CSV file corresponds to a distinct pairing of an observatory with a photometric band and includes derived statistical parameters (mean and standard deviation) of the magnitude difference between observed apparent magnitudes and computed V-band magnitudes, calculated from a specified number of observations, as detailed in Table \ref{tab:observatory_band_magnitude}.

\subsection{Magnitude range analysis}

We also analyzed the distribution of asteroid magnitudes, which is presented in the figure \ref{fig:magnitude_distribution}. The figure shows a set of histograms representing the distribution of observed magnitudes of asteroids. The magnitudes are plotted on the x-axis, while the y-axis represents normalized counts, which likely indicates the relative frequency of observations at each magnitude level.

\begin{figure}[h]
\centering
\includegraphics[width=1.0\textwidth]{../figs/mag_distribution.png}
\caption{Distribution of asteroid magnitudes}
\label{fig:magnitude_distribution}
\end{figure}

The peak of each histogram shows the most frequently observed magnitude range for the asteroids as recorded by each observatory and filter. The varying peak positions across the histograms reflects differences in the sensitivity and observational capabilities of different observatories and filters. For example, some observatories might be more adept at detecting fainter asteroids (hence, higher magnitude values), while others may focus on brighter objects (lower magnitude values). 

The observed discretization of stellar magnitude values within the distribution data of asteroids reflects precision limitations of the reported by the observatories and compiled in the Minor Planet Center (MPC) database. This phenomenon is likely due to the fact that the magnitude values are rounded to the nearest integer or one decimal place. This rounding limits the precision of the data, and should be taken into account when analyzing the data.

\subsection{Temporal Analysis of Magnitude Variations in Asteroid Observations Across Different Observatories}

We analyzed the temporal variations in the magnitude of asteroids as observed by different observatories. The figure shows the collection of scatter points and the rolling mean values over time for asteroids observations of different observatories and filters corrected by the mean distribution value obtained in the previous subsection. The x-axis indicates the time in Julian dates minus t0=2445000.5.

The range of the x-axis in each plot shows the temporal coverage of observations from each observatory. Wide ranges suggest long-term observational programs, while narrower ones could indicate shorter observational campaigns. Some observatories continue to observe asteroids, while others have stopped their observations and therefore their data will not change over time.

\begin{figure}[h]
\centering
\includegraphics[width=1.0\textwidth]{../figs/obs_time_distribution.png}
\caption{Over time variations in the magnitude of asteroids as observed by different observatories}
\label{fig:magnitude_time_distribution}
\end{figure}

The normalization of the time range in the observed data allows for the alignment of datasets from multiple sources on a uniform temporal scale. This scaling is motivated by the need to facilitate a coherent analysis across observations that were made at different times with different cadences and durations. The normalization process is particularly useful for detecting systematic behaviors and deviations that could indicate observational events, such as instrumental sensitivity changes, or data processing inconsistencies.

\begin{figure}[h]
\centering
\includegraphics[width=1.0\textwidth]{../figs/obs_time_distribution_norm.png}
\caption{Over time normalized variations in the magnitude of asteroids as observed by different observatories}
\label{fig:magnitude_time_distribution_norm}
\end{figure}

From the figure \ref{fig:magnitude_time_distribution_norm}, we can observe that some of the observatories exhibit consistent magnitude measurements over time, with minimal fluctuations around the mean value of the magnitude difference. This stability suggests that these observatories have maintained a consistent level of precision and accuracy in their observations. In contrast, other observatories display more erratic behavior, with larger deviations from the mean magnitude difference. These deviations could be indicative of changes in observational conditions, equipment, or data processing techniques over time. Therefore these anomalies could be used to correct the data and improve the overall quality of the dataset.

For each distinct observatory and filter, we prepeared the correcting coefficients that was stored in series of CSV files. The file name format
is "obs\_observatory\_band\_uniform.csv", where 
"observatory" is the code of the observatory and "band" is the photometric band of the observation. These CSV files are structured with data across three columns:

\begin{longtable}{lp{10cm}}
    \toprule
    \textbf{Column} & \textbf{Description} \\
    \midrule
    \endhead
    epoch & The Julian Date. \\
    % mag\_linear & The linear magnitude of the observed object, indicative of its brightness at the time of observation. \\
    slope & The slope parameter of piecewise linear fitting function at the epoch of observation.
    \\
    intercept & The intercept parameter of piecewise linear fitting function at the epoch of observation.
    \\
\bottomrule
\caption{Description in the CSV File of the photometry correction coefficients}
\label{tab:piecewise_linear_fitting}
\end{longtable}

These CSV files provides a concise yet comprehensive overview of the observed astronomical object's brightness over time, through parameters obtained from a piecewise linear regression analysis of the magnitude difference data.

\subsection{Correction of the time dependent magnitude differences}

The correction of the time-dependent magnitude differences was performed by taking into account the mean value and piecewise linear fittingparameters of the magnitude differences. 

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{../figs/photometry_correction.png}
    \caption{The photometry correction of the apparent magnitude of observed asteroid data}
    \label{fig:photometry_overtime_correction}
    \end{figure}
    
% The procedure of correcting of the apparent magnitude datasets was performed by subtracting the interpolated rolling mean values from the magnitude differences. 
The example of the correction the data obtained from observatory Haleakala-AMOS is presented in the figure \ref{fig:photometry_overtime_correction}.
The original data is shown in the top panel, while the corrected data is shown in the bottom panel. The corrected data exhibits close to zero mean value of magnitude difference indicated by the red line.

% The corrected data was obtained by subtracting the mean value and the piecewise linear fitting function from the magnitude differences.

\subsection{Correlation Between Apparent Magnitude and Magnitude Differences in Asteroids.}

In our analysis, we investigated the relationship between the apparent magnitudes of observed asteroids and their deviations from the predicted magnitudes. The figures illustrate two distinct examples of the data obtained from the G band of the Catalina Sky Survey and the ATLAS-HKO survey obtained in the "Cyan" filter.

\begin{figure}[h]
\centering
\includegraphics[width=0.45\textwidth]{../figs/observatories/magV_deltaV/obs_703_G.png}
\includegraphics[width=0.45\textwidth]{../figs/observatories/magV_deltaV/obs_T05_o.png}
\includegraphics[width=0.45\textwidth]{../figs/observatories/magV_deltaV/obs_644_0.png}
\includegraphics[width=0.45\textwidth]{../figs/observatories/magV_deltaV/obs_F51_g.png}
\caption{Magnitude differences as a function of apparent magnitude for different observatories and filters. White error bars indicate the standard deviation of the median magnitude differences.}
\label{fig:magnitude_difference_apparent_magnitude}
\end{figure}

Here we can clearly observe that some observatories exhibit a clear deviation of the magnitude differences in the apparent magnitude of the observed asteroids especially for bright sources, which probably indicates an overestimation of the magnitude of the bright sources. The data from the Catalina survey show clear evidence of change in the behavior of the bright sources brighter than 12 mag, while the data from the ATLAS survey show a stable behavior in the whole range of presented observational data. At the same time, the Palomar data show a non-linear behavior of the magnitude differences with the apparent magnitude of the observed asteroids in the whole range of the presented data.
% This analysis sets the magnitude constraints and corrections for some observatories and filters and will be used to correct the final dataset.

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{../figs/observatories/magV_deltaV/obs_703_G_corrected.png}
\caption{The origin (top) and corrected (bottom) magnitude differences as a function of apparent magnitude for the Catalina Sky Survey in the G band.}
\label{fig:magnitude_difference_apparent_magnitude_corrected}
\end{figure}

\subsection{Correction of the magnitude differences from the apparent magnitude}

The correction procedure for magnitude differences from apparent magnitude involves a segmented analysis of observed asteroid data based on brightness levels. Initially, the dataset is divided into slices with a step size of $S$ magnitudes within the identified range of apparent magnitudes. For each slice, a window of size $W>=S$ is defined to conduct a detailed statistical analysis. Within this window, the mean, median, and standard deviation (std) of the magnitude differences are computed. This methodical approach allows for a nuanced examination of how magnitude differences vary across different brightness levels. In Figure \ref{fig:magnitude_difference_apparent_magnitude}, an error bar plot visualizes the median and standard deviation (std) values, calculated for data segments with a step size of 0.2 mag and a window size of 0.4 mag. These statistical measures were derived from subsamples containing over 100 observations each.


\subsection{Photometric Calibration of Asteroid Observations}

The example of the power distribution of the magnitude differences for original and corrected over the time data is presented in the figure \ref{fig:photometry_correction_power_distribution}. The revised dataset demonstrates a minor deviation from the mean value of zero, attributable to the non-Gaussian distribution characteristic of the initial data. Furthermore, a small decrease in the variance of magnitude differences is observed, signifying a notable enhancement in the dataset's quality.

\begin{figure}[h]
\centering
\includegraphics[width=1.0\textwidth]{../figs/uncertainties/photometry_uncertainties_10.png}
\caption{The power distribution of the magnitude differences for original and corrected over the time data}
\label{fig:photometry_correction_power_distribution}
\end{figure}


% \begin{equation}
%     m_{corr} = m_{obs} - 5 \log_{10} \left( \frac{r_{\oplus} \cdot r_{\odot}}{r_{\oplus} + r_{\odot}} \right)
% \end{equation}
    
% where $m_{corr}$ is the corrected magnitude, $m_{obs}$ is the observed magnitude, $r_{\oplus}$ is the distance from the Earth to the Sun, and $r_{\odot}$ is the distance from the Earth to the asteroid.

\subsection{Astrometry analysis of asteroids observations}

TODO: Compare the observed positions of asteroids with the predicted positions to determine the accuracy of the observations. The predicted positions were calculated using the IMCC Miriad ephemeris service, which provides ephemeris data for asteroids and other celestial bodies, while the observed positions were obtained from the MPC dataset. The differences between the observed and predicted positions were then analyzed to assess the accuracy of the observations.

\section{Determination of the H, G1, G2 parameters for the observed asteroids}

TODO: The H, G1 and HG2 parameters are used to describe the phase curve of an asteroid, which is the variation in brightness with phase angle. The H parameter is the absolute magnitude of the asteroid, which is a measure of its intrinsic brightness. The G1 parameter describes the slope of the phase curve, while the HG2 parameter describes the curvature. These parameters are important for understanding the physical properties of asteroids, such as their surface roughness and composition. We determined the H, G1 and HG2 parameters for the observed asteroids by fitting the phase curve model to the observed magnitudes as a function of phase angle. The phase curve model is given by the equation:

% \begin{equation}
%     m(\alpha) = H + 5 \log_{10} \left( \frac{1}{\cos(\alpha)} \right) + HG1 \cdot \alpha + HG2 \cdot \alpha^2
% \end{equation}

% where $m(\alpha)$ is the apparent magnitude of the asteroid as a function of phase angle $\alpha$, $H$ is the absolute magnitude of the asteroid, and $HG1$ and $HG2$ are the phase curve parameters. We used a least squares fitting algorithm to determine the best-fit values of $H$, $G1$, and $G2$ for each observed asteroid. The results of the fitting process are presented in the figure \ref{fig:HG1G2_fitting}.
% The figure shows the observed magnitudes of the asteroids as a function of phase angle, along with the best-fit phase curve model. The best-fit values of $H$, $HG1$, and $HG2$ are also indicated in the legend. The residuals of the fitting process are presented in the bottom panel, showing the differences between the observed magnitudes and the best-fit model. The residuals are distributed around zero, indicating a good fit to the data.

\begin{figure}[h]
\centering
\includegraphics[width=1.0\textwidth]{../figs/hg1g2_models.png}
\caption{The phase light curves of asteroids that colored by the G1, and G2 values. The color bars show the range of G1 (left) and G2 (right) parameters.}
\label{fig:G1G2_models}
\end{figure}

\subsection{The BFT distribution of the asteroids H, G1, G2 parameters}

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{../figs/g1g2_bft_distribution.png}
\caption{The BFT distribution of the asteroids G1, G2 parameters}
\label{fig:G1G2_bft_distribution}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{../figs/hg1g2data/fit_10002.png}
\caption{The fitting of the phase curve model to the observed magnitudes of the asteroid number 10002}
\label{fig:HG1G2_fitting}
\end{figure}


\subsection{The modeling of the accuracy H,G1,G2 parameters}

The determination of accuracy of H, G1 and G2 parameters was mage by analyzing the differences between the modeled data sample where the H, G1 and HG2 parameters as well as the number of observations, minimal phase angle and the magnitude variations of the observed asteroids were determined. And then the differences between the modeled and fitted H, G1, G2 parameters were analyzed. The results of the analysis are presented in the figures \ref{fig:H_error_statistics}, \ref{fig:G1G2_error_statistics}. The figures show the distribution of the residuals of the fitting process for the absolute magnitude H, the G1 parameter, and the G2 parameter. The residuals are distributed around zero, indicating a good fit to the data.

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{../figs/H_error_statistics.png}
\caption{The distribution of the residuals of the fitting process for the absolute magnitude H}
\label{fig:H_error_statistics}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.45\textwidth]{../figs/G1_error_statistics.png}
\includegraphics[width=0.45\textwidth]{../figs/G2_error_statistics.png}
\caption{The distribution of the residuals of the fitting process for the G1, G2 parameter}
\label{fig:G1G2_error_statistics}
\end{figure}

% \begin{figure}[h]
% \centering
% \caption{The distribution of the residuals of the fitting process for the G2 parameter}
% \label{fig:G2_error_statistics}
% \end{figure}

\section{Conclusion}
% Your content here

\end{document}
